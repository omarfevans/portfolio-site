import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '../custom.scss'
import NavBar from '../components/NavBar'
import Home from '../components/Home'
import ArticlesHome from '../components/ArticlesHome'
import Article from '../components/Article'
import Experience from '../components/Experience'
import Videos from '../components/Videos'
import ContactForm from '../components/ContactForm'

Vue.use(Router);
Vue.use(BootstrapVue);
Vue.component('nav-bar', NavBar);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: { title: 'Home' }
        },
        {
            path: '/articles/',
            name: 'articles',
            component: ArticlesHome,
            meta: { title: 'Articles' }
        },
        {
            path: '/articles/:slug',
            name: 'article',
            component: Article,
            meta: { title: 'Article' }
        },
        {
            path: '/experience',
            name: 'experience',
            component: Experience,
            meta: { title: 'Experience' }
        },
        {
            path: '/videos',
            name: 'videos',
            component: Videos,
            meta: { title: 'Videos' }
        },
        {
            path: '/contact',
            name: 'contact',
            component: ContactForm,
            meta: { title: 'Contact Me' }
        }
    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})