import Vue from 'vue'
import App from './App.vue'
import router from './router'

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next()
})

let APIURL = 'localhost:8080';
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
